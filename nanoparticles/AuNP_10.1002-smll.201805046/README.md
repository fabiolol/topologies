![Scheme](../doc/TOC.jpg)

Please cite us!

```
@article{Lolicato2019,
author = {\textbf{Lolicato, Fabio} and Joly, Loic and Martinez-Seara, Hector and Fragneto, Giovanna and Scoppola, Ernesto and {Baldelli Bombelli}, Francesca and Vattulainen, Ilpo and Akola, Jaakko and Maccarini, Marco},
issn = {16136829},
journal = {Small},
keywords = {gold nanoparticles,lipid membranes,molecular dynamics simulations,nanotoxicity,neutron reflectometry},
month = {apr},
number = {23},
pages = {1805046},
publisher = {John Wiley {\&} Sons, Ltd},
title = {{The Role of Temperature and Lipid Charge on Intake/Uptake of Cationic Gold Nanoparticles into Lipid Bilayers}},
url = {https://onlinelibrary.wiley.com/doi/abs/10.1002/smll.201805046},
volume = {15},
year = {2019}
}
```


